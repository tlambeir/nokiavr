﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
//using UnityEditor;

public class WallInspectManager : MonoBehaviour {

	public string correctType;
	public ScoreManager scoreManager;
	public IndentifyController IndentifyController;

	private Ray ray;
	private RaycastHit hit;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		//correctType = wallInfoPanel.GetComponent<WallInfoPanelManager>().identifiedModel.wallType;
		WallTypeButtonManager wallTypeButtonManager = buttonRaycast();
		if (wallTypeButtonManager) {
			// change color of button
			wallTypeButtonManager.GetComponent<Button>().Select();
		}
		if (Input.GetMouseButtonDown (0) ||OVRInput.GetDown(OVRInput.RawButton.A)) {
			WallTypeButtonManager selecteDwallTypeButtonManager = buttonRaycast();
			if (selecteDwallTypeButtonManager && !selecteDwallTypeButtonManager.isContinueButton && !selecteDwallTypeButtonManager.isKnockAgainButton) {
				selectWallType (selecteDwallTypeButtonManager.type, selecteDwallTypeButtonManager.identifiedModel);
			} else if (selecteDwallTypeButtonManager && selecteDwallTypeButtonManager.isKnockAgainButton) {
				selecteDwallTypeButtonManager.identifiedModel.playSound();
			} else if (selecteDwallTypeButtonManager && selecteDwallTypeButtonManager.isContinueButton) {
				selecteDwallTypeButtonManager.identifiedModel.infoPanel.SetActive (false);
				if (selecteDwallTypeButtonManager.identifiedModel && selecteDwallTypeButtonManager.identifiedModel.message) {
					selecteDwallTypeButtonManager.identifiedModel.message.SetActive (false);
					selecteDwallTypeButtonManager.identifiedModel.isBeingIdentified = false;
				}
					
				IndentifyController.snapShotHandlerActive = true;
			}
		}
	}

	public WallTypeButtonManager buttonRaycast (){
		ray = new Ray(transform.position, transform.forward);
		WallTypeButtonManager wallTypeButtonManager = null;
		if (Physics.Raycast(ray, out hit, 100))
		{
			wallTypeButtonManager = hit.collider.GetComponent<WallTypeButtonManager> ();
			return wallTypeButtonManager;
		}
		return null;
	}

	public void selectWallType(string selectedType, IdentifyModel identifiedModel){
		if (selectedType == (identifiedModel.isDensity ? identifiedModel.densityType : identifiedModel.wallType)) {	// correct
			if (identifiedModel.isDensity) {	// identify density
				scoreManager.assessDensity ();	// ~ densities assessed ++
			} else { // identify wall type
				scoreManager.inspectWall ();	// ~ wall inspected ++
			}
			WallTypeButtonManager chosenBtnMngr = buttonRaycast ();
			SpriteState st = chosenBtnMngr.gameObject.GetComponent<Button> ().spriteState;
			st.highlightedSprite = chosenBtnMngr.correctSprite;
			chosenBtnMngr.gameObject.GetComponent<Image> ().sprite = chosenBtnMngr.correctSprite;
			chosenBtnMngr.gameObject.GetComponent<Button> ().spriteState = st;
			identifiedModel.wallSelectionCanvas.SetActive (false);
			identifiedModel.infoPanel.SetActive (true);
			identifiedModel.isIdentified = true;
			identifiedModel.isBeingIdentified = false;
		} else {	// wrong
			WallTypeButtonManager chosenBtn = buttonRaycast ();
			Debug.Log (chosenBtn.type);
			chosenBtn.gameObject.GetComponent<Button> ().interactable = false;
//			identifiedModel.wallSelectionCanvas.SetActive (false);
//			identifiedModel.message.SetActive (true);

		}
	}
}
