﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SnapShotHandler : MonoBehaviour {
    public float flashLength = 2;
    private float flashCounter = 2;
	public bool playSound = false;
    // Use this for initialization
	
	// Update is called once per frame
	void Update () {
        flashCounter += Time.deltaTime;
		Image image;
		image = GetComponent<Image> ();
		Color temp = image.color;
		if (flashCounter <= flashLength) {
			temp.a = flashCounter;
			image.color = temp;
		} else {

			temp.a = 0;
			image.color = temp;
		}
    }

	public void fadeOut()
    {
        flashCounter = 0;
		if (playSound) {
			AudioSource audio = GetComponent<AudioSource>();
			audio.Play();
		}
    }
}
