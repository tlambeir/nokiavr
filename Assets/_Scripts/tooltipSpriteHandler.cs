﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class tooltipSpriteHandler : MonoBehaviour {

	public Sprite defaultSprite;
	public Sprite activeSprite;

	// Use this for initialization
	void Start () {
		reset();
	}


	public void setActive(){
		//GetComponent<SpriteRenderer> ().sprite = activeSprite;
		GetComponent<Image> ().sprite = activeSprite;
	}

	public void reset(){
		//GetComponent<SpriteRenderer> ().sprite = defaultSprite;
		GetComponent<Image> ().sprite = defaultSprite;
	}

}
