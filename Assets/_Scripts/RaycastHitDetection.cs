﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RaycastHitDetection : MonoBehaviour
{

    private Ray ray;
    private RaycastHit hit;
    private GameObject bumbleBee;

    // Use this for initialization
    void Start()
    {
       
    }

    // Update is called once per frame
    void Update()
    {
        ray = new Ray(transform.position, transform.forward);
        if (Physics.Raycast(ray, out hit, 100))
        {
            //check if it hits desired object
            // and do some logic
            if (hit.collider.name == "Poster_013")
            {
                Debug.Log(hit.collider.name);
            }
        }
    }
}