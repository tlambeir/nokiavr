﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class XboxControllerInput : MonoBehaviour {
	Canvas objectives;
	Text text;
	// Use this for initialization
	void Start () {
		objectives = gameObject.transform.Find ("OVRCameraRig/TrackingSpace/CenterEyeAnchor/HudCanvas").GetComponent<Canvas>();
		objectives.enabled = false;
		text = gameObject.transform.Find ("OVRCameraRig/TrackingSpace/CenterEyeAnchor/HudCanvas/Panel/Title").GetComponent<Text>();
	}
	
	// Update is called once per frame
	void Update () {

		if (OVRInput.Get (OVRInput.Button.Four)) { // Button LShoulder
			objectives.enabled = true;
		} else{
			objectives.enabled = false;
		}
	}
}
