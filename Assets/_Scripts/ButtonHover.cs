﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonHover : MonoBehaviour {

	public int timeRemaining = 5;
	public GameObject ball;

	void countDown(){
		timeRemaining--;
		print (timeRemaining);

		if (timeRemaining <= 0) {
			playVideo ();

			CancelInvoke ("countDown");
			timeRemaining = 5;
			print ("Reset time");
		}
	}

	void playVideo(){
		print ("Finished countdown");
		Vector3 newPos = transform.position;
	}

	public void MouseOver(){
		InvokeRepeating ("countDown",1 , 1);
	}
	public void MouseOut(){
		CancelInvoke ("countDown");
		timeRemaining = 5;
	}
}
