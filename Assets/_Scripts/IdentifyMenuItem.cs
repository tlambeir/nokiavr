﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityStandardAssets.Characters.FirstPerson;
using UnityEngine.SceneManagement;

public class IdentifyMenuItem : MonoBehaviour {

	public SnapShotHandler snapShotHandler;
	public bool m_ShowDebugRay;                   // Optionally show the debug ray.
	public float m_DebugRayLength = 1000f;        // Debug ray length.
	public float m_DebugRayDuration = 1f;         // How long the Debug ray will remain visible.
	public float m_RayLength = 1000f;             // How far into the scene the ray is cast.


	// public FirstPersonController FPSController;

	private Ray ray;
	private RaycastHit hit;
	private IdentifyMenu identifiedMenu;
	private bool isActive = false;
	public bool snapShotHandlerActive = true;

	// Use this for initialization
	void Start () {
		Cursor.visible = false;
	}

	public void continueIndentifying()
	{
		snapShotHandlerActive = true;
		// FPSController.enabled = true;
	}

	// Update is called once per frame
	void Update () {
			IdentifyMenu menuButtonManager = buttonRaycast ();
			if (menuButtonManager) {
				menuButtonManager.GetComponent<Button> ().Select ();
			} else {
				GameObject myEventSystem = GameObject.Find ("EventSystem");
					myEventSystem.GetComponent<UnityEngine.EventSystems.EventSystem> ().SetSelectedGameObject (null);
			}
		if (Input.GetMouseButtonDown(0) || OVRInput.GetDown(OVRInput.RawButton.A))
			{
				IdentifyMenu identifiedMenu = buttonRaycast();
				if (identifiedMenu) {
					// change color of button
					if (identifiedMenu)
					{
						SpriteState spriteState = new SpriteState();
						spriteState = identifiedMenu.GetComponent<Button> ().spriteState;
						spriteState.highlightedSprite = identifiedMenu.LoadingSprite;
						identifiedMenu.GetComponent<Button> ().spriteState = spriteState;
						StartCoroutine (HandleClick (identifiedMenu));

					}
				} 
			}
	}

	public IdentifyMenu buttonRaycast (){
		ray = new Ray(transform.position, transform.forward);
		IdentifyMenu menuButtonManager = null;

		// Show the debug ray if required
		if (m_ShowDebugRay)
		{
			var hitting = Physics.Raycast(ray, out hit, m_RayLength);
			Color color = hitting ? Color.green : Color.red;
			Debug.DrawRay(transform.position, transform.forward * m_DebugRayLength, color, m_DebugRayDuration);
		}

		if (Physics.Raycast(ray, out hit, m_RayLength))
		{
			menuButtonManager = hit.collider.GetComponent<IdentifyMenu> ();
			return menuButtonManager;
		}
		return null;
	}

	IEnumerator HandleClick(IdentifyMenu identifiedMenu) {
		yield return new WaitForSeconds(0.25f);
		Debug.Log("Load Scene: "+identifiedMenu.sceneToLoad);
		SceneManager.LoadSceneAsync(identifiedMenu.sceneToLoad);
	}
}