﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRStandardAssets.Utils;
using UnityEngine.UI;

public class CubeCanvasController : MonoBehaviour {
	
	[SerializeField] private VRInteractiveItem m_InteractiveItem;
	private Text cubeText;

	void Start () {
		cubeText = gameObject.transform.Find ("CubePanel/Text").GetComponent<Text>();
		cubeText.text = "lol";
	}
	private void Awake ()
	{
//		m_Renderer.material = m_NormalMaterial;
	}


	private void OnEnable()
	{
		m_InteractiveItem.OnOver += HandleOver;
		m_InteractiveItem.OnOut += HandleOut;
		m_InteractiveItem.OnClick += HandleClick;
		m_InteractiveItem.OnDoubleClick += HandleDoubleClick;
	}


	private void OnDisable()
	{
		m_InteractiveItem.OnOver -= HandleOver;
		m_InteractiveItem.OnOut -= HandleOut;
		m_InteractiveItem.OnClick -= HandleClick;
		m_InteractiveItem.OnDoubleClick -= HandleDoubleClick;
	}


	//Handle the Over event
	private void HandleOver()
	{
		Debug.Log("Show over state: " + cubeText.text);
//		m_Renderer.material = m_OverMaterial;
		if (!enabled) {
			enabled = true;
		}
		cubeText.text = "You are looking at me";


	}


	//Handle the Out event
	private void HandleOut()
	{
		Debug.Log("Show out state");
//		m_Renderer.material = m_NormalMaterial;
	}


	//Handle the Click event
	private void HandleClick()
	{
		Debug.Log("Show click state on canvas" + cubeText.text);
		if (!enabled) {
			enabled = true;
		}
		cubeText.text = "You clicked on me";
//		m_Renderer.material = m_ClickedMaterial;
//		canvas.enabled = !canvas.enabled;
	}


	//Handle the DoubleClick event
	private void HandleDoubleClick()
	{
		Debug.Log("Show double click");
//		m_Renderer.material = m_DoubleClickedMaterial;
		if (!enabled) {
			enabled = true;
		}
		cubeText.text = StrikeThrough("You double clicked on me");
	}

	// Utilities:
	public string StrikeThrough(string s)
	{
		string strikethrough = "";
		foreach (char c in s)
		{
			strikethrough = strikethrough + c + '\u0336';
		}
		return strikethrough;
	}
}
