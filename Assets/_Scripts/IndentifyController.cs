﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityStandardAssets.Characters.FirstPerson;

public class IndentifyController : MonoBehaviour {
    
	public SnapShotHandler snapShotHandler;
    public GameObject ObstructionPanel;
	public GameObject messagePanel;
    public Text infoPanelText;
    public Text infoPanelDescription;
	public ScoreManager scoreManager;


    // public FirstPersonController FPSController;

    private Ray ray;
    private RaycastHit hit;
    private IdentifyModel identifiedModel;
    public bool snapShotHandlerActive = true;
    // Use this for initialization
    void Start () {
        Cursor.visible = false;
    }
		
    public void continueIndentifying()
    {
		ObstructionPanel.SetActive(false);
		messagePanel.SetActive(false);
        snapShotHandlerActive = true;
        // FPSController.enabled = true;
    }

    // Update is called once per frame
    void Update () {

		if (Input.GetMouseButtonDown(0) || OVRInput.GetDown(OVRInput.RawButton.A) || OVRInput.GetDown(OVRInput.RawButton.Y))
        {
            if (snapShotHandlerActive){
                ray = new Ray(transform.position, transform.forward);
                if (Physics.Raycast(ray, out hit, 100))
                {
                    identifiedModel = hit.collider.GetComponent<IdentifyModel>();
                    
					if (identifiedModel && identifiedModel.isHazard && !identifiedModel.isDensity && !identifiedModel.isIdentified && !identifiedModel.isBeingIdentified) 
                    {
						identifiedModel.GetComponent<ObjectDescriptionController> ().hazardBackground.setActive ();
						StartCoroutine (HandleClick (identifiedModel));

                    }
					else if (identifiedModel && !identifiedModel.isHazard && !identifiedModel.isIdentified && !identifiedModel.isDensity)
                    {
                        Debug.Log("This is not an obstruction");
						StartCoroutine (HandleClick (identifiedModel));
                    }
                }
            }
        }
    }

	IEnumerator HandleClick(IdentifyModel identifiedModel) {
		yield return new WaitForSeconds(0.25f);
		identifiedModel.playSound ();
		if (identifiedModel.isWall) {
			yield return new WaitForSeconds (2.5f);
			identifiedModel.isBeingIdentified = true;
			identifiedModel.wallSelectionCanvas.SetActive (true);
		} else {
			if (identifiedModel.isHazard) {
				identifiedModel.isIdentified = true;
				scoreManager.identifyObstruction ();
				IdentifyModel identifyModel = hit.collider.GetComponent<IdentifyModel>();
				if (identifiedModel.infoPanel) {
					identifyModel.infoPanel.SetActive (true);
				}
			} else {
				identifiedModel.isIdentified = true;
			}
		}
	}
}
