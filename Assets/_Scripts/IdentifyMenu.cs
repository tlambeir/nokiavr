﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class IdentifyMenu : MonoBehaviour {

	public string title;
	private Text titleText;
	public string sceneToLoad;
	private Text sceneToLoadText;
	public Sprite LoadingSprite;


	// Use this for initialization
	void Start () {
		if(titleText)
			titleText.text = title;
		if(sceneToLoadText)
			sceneToLoadText.text = sceneToLoad;
	}

	// Update is called once per frame
	void Update () {


	}

}