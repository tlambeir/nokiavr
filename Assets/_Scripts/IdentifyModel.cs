﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using HighlightingSystem;

public class IdentifyModel : HighlighterBase {

	public bool isHazard;
	public bool isWall;
	public bool isDensity;
	public string wallType;
	public string densityType;
	public GameObject wallSelectionCanvas;
	public bool isIdentified;
	public bool isBeingIdentified;
	public GameObject infoPanel;
	public GameObject message;


	public AudioSource sound;

	public string title;
	public Text titleText;
	public string description;
	public Text descriptionText;

	private Highlighter highlighter;


	// Use this for initialization
	new void Start () {
		if(titleText)
		titleText.text = title;
		if(descriptionText)
		descriptionText.text = description;

		highlighter = GetComponent<Highlighter> ();

		// Fade in constant highlighting
		highlighter.ConstantOn(Color.yellow);

		// Turn off constant highlighting
		highlighter.ConstantOffImmediate();

		// Start flashing from blue to cyan color and frequency = 2f
		highlighter.FlashingOn(Color.yellow, Color.white, 0.5f);

		highlighter.SeeThroughOff ();
	}

	// Update is called once per frame
	new void Update () {
		if (highlighter && (isIdentified || isBeingIdentified)) {
			highlighter.Off();
		}
	}

	public void playSound(){
		if (sound)
		sound.Play();
	}
}
