﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InfoPanelController : MonoBehaviour {

	public IdentifyModel IdentifyModel;
	private IndentifyController IndentifyController;
	private GameObject Player;
	public GameObject panel;

	// Use this for initialization
	void Start () {
		
	}

	void Update(){
		if(!Player)
			Player = FindObjectOfType<SceneController> ().getActivePlayerObject ();
		float distance = Vector3.Distance (Player.GetComponent<Transform> ().position, transform.position);

		Debug.Log (IdentifyModel.isIdentified);
		Debug.Log (distance);
		Debug.Log ("--");

		if (IdentifyModel.isIdentified) {
			if (distance <= 8) {
				panel.SetActive (true);
			} else {
				panel.SetActive (false);
			}
		}
	}
}
