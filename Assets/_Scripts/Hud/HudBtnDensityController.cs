﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HudBtnDensityController : MonoBehaviour {
	public bool status;	// false: need to determine - true: already determined room density

	public Canvas choicesCanvas;
	public Camera fpsCam;
	public CharacterController charController;
	public float shiftDistance;
	// Use this for initialization
	void Start () {
		status = false;
		shiftDistance = 1.5f;
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKeyDown("[4]") || Input.GetMouseButtonDown(1)){
			Debug.Log("Keypad 4 pressed");

			choicesCanvas.transform.position = fpsCam.transform.position + fpsCam.transform.forward * shiftDistance;
			// todo: control shiftDistance in cases where the object is too close to the camera and get over the UI. possible to check the distance from camera-object??
			choicesCanvas.transform.rotation = fpsCam.transform.rotation;
			choicesCanvas.enabled = true;
			// todo: disable movement when showing the choices canvas

			// todo config buttons

			
		}
	}
}
