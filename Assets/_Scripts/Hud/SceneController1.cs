﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneController1 : MonoBehaviour {

	public bool isVR;
	private GameObject ActivePlayerObject;
	public GameObject VRController;
	public GameObject VRPlayerObject;
	public GameObject PCController;
	public GameObject PCPlayerObject;

	public GameObject ObjectiveUI;
	public bool ObjectiveUIStatus;

	// Use this for initialization
	void Start () {
		setObjectiveUIStatus ();
		Cursor.lockState = CursorLockMode.Confined;
		QualitySettings.antiAliasing = 4;
		ActivePlayerObject = isVR ? VRPlayerObject : PCPlayerObject;
		if (isVR)
			VRController.SetActive (true);
		else
			PCController.SetActive (true);
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown("space") || Input.GetKeyDown ("joystick button 5")) {
			SceneManager.LoadScene (SceneManager.GetActiveScene ().buildIndex);
		}
		if (Input.GetKeyDown("tab") || Input.GetKeyDown ("joystick button 4")) {
			setObjectiveUIStatus ();
		}
	}

	void setObjectiveUIStatus(){
		if (ObjectiveUIStatus) {
			ObjectiveUI.SetActive (false);
			ObjectiveUIStatus = false;
		} else {
			ObjectiveUI.SetActive (true);
			ObjectiveUIStatus = true;
		}
	}

	public GameObject getActivePlayerObject(){
		return ActivePlayerObject;
	}
}
