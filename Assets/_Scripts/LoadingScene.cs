﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class LoadingScene : MonoBehaviour {

	// Use this for initialization
	void Start () {

		if (PlayerPrefs.GetString("LevelName") == "") {
			PlayerPrefs.SetString ("LevelName","_Scenes/DemoLevel");
		}

		StartCoroutine (LoadLevel());

	}

	// Update is called once per frame
	void Update () {

	}


	IEnumerator LoadLevel() {
		yield return new WaitForSeconds(1);
		SceneManager.LoadSceneAsync(PlayerPrefs.GetString ("LevelName"));

	}



}
