﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour {
	public Text message;
	public int obstructionsToIdentify;
	public int wallsToInspect;
	public int densitiesToInspect;
	public string density;
	public List<string> densityOptions = new List<string>();

	public int obstructionsIdentified;
	public Text obstructionsIdentifiedText;
	public int wallsInspected;
	public Text wallsInspectedText;
	public int densitiesAssessed;
	public Text densitiesAssessedText;

	public SceneController SceneController;


	// Use this for initialization
	void Start () {
		obstructionsIdentified = 0;
		wallsInspected = 0;
		densitiesAssessed = 0;
	}

	public void identifyObstruction(){
		obstructionsIdentified++;
	}

	public void inspectWall(){
		wallsInspected++;
	}

	public void assessDensity(){
		densitiesAssessed++;
	}
	
	// Update is called once per frame
	void Update () {
		if (obstructionsIdentified >= obstructionsToIdentify && wallsInspected >= wallsToInspect && densitiesAssessed >= densitiesToInspect) {
			// show scoring screen
			Debug.Log("obs_ID = " + obstructionsIdentified + ", obs_to_ID = " + obstructionsToIdentify + 
				"; walls_Insp = " + wallsInspected + ", walls_to_Insp = " + wallsInspected +
				"; density_assessed = " + densitiesAssessed + ", density_to_assess = " + densitiesToInspect );
			SceneController.endGame();
		}
		obstructionsIdentifiedText.text = obstructionsIdentified.ToString ();
		wallsInspectedText.text = wallsInspected.ToString ();
		densitiesAssessedText.text = densitiesAssessed.ToString ();
	}
}
