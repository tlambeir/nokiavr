﻿using UnityEngine;
using System.Collections;

public class Rotation_Script : MonoBehaviour {

	[SerializeField]
	private float rotationSpeed;

	public float RotationSpeed
	{
		get {return this.rotationSpeed;}
		set {rotationSpeed = value;}
	}


	// Use this for initialization
	void Start () {
	if (this.rotationSpeed == null)
				this.RotationSpeed = 50f;
	}
	
	// Update is called once per frame
	void Update () {
			transform.Rotate(0f, this.rotationSpeed * Time.deltaTime, 0f);
	}
}
