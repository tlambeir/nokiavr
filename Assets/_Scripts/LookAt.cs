﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookAt : MonoBehaviour {

    public Transform target;

    void Update()
    {
        if (target != null)
        {
			Vector3 pos = transform.position + target.transform.rotation * Vector3.forward;
			transform.LookAt(
				pos,
				target.transform.up
            );
        }
    }
}
