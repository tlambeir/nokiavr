﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SphereController : MonoBehaviour {
//	public OVRPlayerController ovrpc;
		public Camera ovrpc;
	private Canvas canvas;
	private Text text;

	void Start () {
		text = gameObject.transform.Find ("SphereCanvas/SpherePanel/Text").GetComponent<Text>();
		canvas = gameObject.transform.Find ("SphereCanvas").GetComponent<Canvas>();
		canvas.enabled = false;
	}
	
	void Update () {
		float distance = Vector3.Distance(ovrpc.transform.position, transform.position);

		if (distance < 3.5) {
			text.text = "Distance:" + distance;
			canvas.enabled = true;
		} else {
			canvas.enabled = false;
		}
	}
}
