﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ObjectDescriptionController : MonoBehaviour {

    public GameObject toolTip;
	public tooltipSpriteHandler hazardBackground;
	public GameObject hazardTooltip;
    public Text hazardDescription;
	public GameObject safeTooltip;
	public Text safeDescription;
	private Text activeDescription;
	private GameObject Player;
	private Ray ray;
	private RaycastHit hit;
	public Transform distanceMarker;

    // Use this for initialization
    void Start () {
		bool isHazard = GetComponent<IdentifyModel> ().isHazard;
		bool isWall = GetComponent<IdentifyModel> ().isWall;
		activeDescription =  isHazard ? hazardDescription : safeDescription;
		if (isHazard) {
			if(safeTooltip)
				safeTooltip.SetActive (false);
			if (isWall) {
				activeDescription.text = "Knock on the wall to identify what it is made of.";
			} else {
				activeDescription.text = "This “<name>” appears to interfere with the signal!\n This has to be documented.";
			}
		} else {
			hazardTooltip.SetActive (false);
			activeDescription.text = "It’s just a “<name>”… No problems here!";
		}
		activeDescription.text = activeDescription.text.Replace("<name>", GetComponent<IdentifyModel>().title);
    }
	
	// Update is called once per frame
	void Update () {
		IdentifyModel IdentifyModel = GetComponent<IdentifyModel> ();
		bool isDensity = IdentifyModel.isDensity;
		if(!Player)
			Player = FindObjectOfType<SceneController> ().getActivePlayerObject ();
		float distance = Vector3.Distance (Player.GetComponent<Transform> ().position, distanceMarker.position);
		if (!IdentifyModel.isBeingIdentified && !IdentifyModel.isIdentified)
		{
			if (distance <= 8 && !isDensity) {
				toolTip.SetActive (true);
				if (!IdentifyModel.isHazard) {
					//IdentifyModel.isIdentified = true;
				}
			} else if (isDensity && distance <= 8) {
				IdentifyModel.isBeingIdentified = true;
				IdentifyModel.wallSelectionCanvas.SetActive (true);
			}
			else{
				disableMesh(IdentifyModel, distance);
			}
		}
		else
		{
			disableMesh(IdentifyModel, distance);
		}
    }

	void disableMesh(IdentifyModel IdentifyModel, float distance)
    {
        toolTip.SetActive(false);
		hazardBackground.reset ();
		// IdentifyModel.isBeingIdentified = false;
		if (IdentifyModel.isDensity && distance > 8) {
			IdentifyModel.isBeingIdentified = false;
			IdentifyModel.wallSelectionCanvas.SetActive (false);
		}
    }
}
