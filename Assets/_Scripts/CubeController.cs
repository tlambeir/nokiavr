﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.Events;
using UnityEngine.Serialization;
using System;
using VRStandardAssets.Utils;

public class CubeController : MonoBehaviour {

//	public OVRPlayerController ovrpc;
	public Camera ovrpc;
	private Canvas cubeCanvas;
	private Text cubeText;
	[SerializeField] private VRInteractiveItem m_InteractiveItem;


	// Use this for initialization
	void Start () {
		cubeText = gameObject.transform.Find ("CubeCanvas/CubePanel/Text").GetComponent<Text>();
		cubeCanvas = gameObject.transform.Find ("CubeCanvas").GetComponent<Canvas>();
//		cubeCanvas.enabled = false;
	}
	
	// Update is called once per frame
	void Update () {
//		float distance = Vector3.Distance(cam.transform.position, transform.position);
		float distance = Vector3.Distance(ovrpc.transform.position, transform.position);

		if (distance < 3.5) {
//			cubeText.text = "Distance:" + distance;
			cubeCanvas.enabled = true;
		} else {
			cubeCanvas.enabled = false;
		}
	}
	private void OnEnable()
	{
		m_InteractiveItem.OnOver += HandleOver;
		m_InteractiveItem.OnOut += HandleOut;
		m_InteractiveItem.OnClick += HandleClick;
		m_InteractiveItem.OnDoubleClick += HandleDoubleClick;
	}


	private void OnDisable()
	{
		m_InteractiveItem.OnOver -= HandleOver;
		m_InteractiveItem.OnOut -= HandleOut;
		m_InteractiveItem.OnClick -= HandleClick;
		m_InteractiveItem.OnDoubleClick -= HandleDoubleClick;
	}
	//Handle the Over event
	private void HandleOver()
	{
		Debug.Log("Show over state");
//		transform.localScale = new Vector3 (3, 3, 3);
//		m_Renderer.material = m_OverMaterial;
	}


	//Handle the Out event
	private void HandleOut()
	{
		Debug.Log("Show out state");
		transform.localScale = new Vector3 (1,1,1);

//		m_Renderer.material = m_NormalMaterial;
	}


	//Handle the Click event
	private void HandleClick()
	{
		Debug.Log("Show click state");
//		m_Renderer.material = m_ClickedMaterial;
//		canvas.enabled = !canvas.enabled;
	}


	//Handle the DoubleClick event
	private void HandleDoubleClick()
	{
		Debug.Log("Show double click");
//		m_Renderer.material = m_DoubleClickedMaterial;
	}
}

