﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UICameraController : MonoBehaviour {

	public Canvas hudCanvas;
	private bool isAtOrigin;

	void Start () {
		hudCanvas.transform.position = transform.position + transform.forward * 5;
		hudCanvas.transform.rotation = transform.rotation;
		hudCanvas.enabled = true;		
		isAtOrigin = true;
	}
	
	// Update is called once per frame
	void Update () {
		// if(OVRInput.Get(OVRInput.Button.One)){
		// 	hudCanvas.enabled = !hudCanvas.enabled;
		// }

		// // Square: show the hudCanvas infront of the camera
		if(OVRInput.GetDown(OVRInput.Button.Three)){
			hudCanvas.transform.position = transform.position + transform.forward * 5;
			hudCanvas.transform.rotation = transform.rotation;
			hudCanvas.enabled = true;
		}
		if(OVRInput.GetUp(OVRInput.Button.Three)){
			hudCanvas.enabled = false;			
		}		
/*
		if(Input.GetAxis("Fire1") == 1){
			hudCanvas.transform.position = transform.position + transform.forward * 5;
			hudCanvas.transform.rotation = transform.rotation;
			hudCanvas.enabled = true;
			isAtOrigin = false;
		}  
		if(Input.GetAxis("Fire1") == 0 && !isAtOrigin){
			hudCanvas.enabled = false;
		}
*/
	}
}
