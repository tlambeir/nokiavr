﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneController : MonoBehaviour {

	public bool isMainMenu = false;
	public bool isVR;
	private GameObject ActivePlayerObject;
	public GameObject VRController;
	public GameObject VRPlayerObject;
	public GameObject PCController;
	public GameObject PCPlayerObject;

	private GameObject ActiveObjectiveUI;
	public GameObject VRObjectiveUI;
	public GameObject PCObjectiveUI;

	public bool ObjectiveUIStatus;

	public string EndScene;

	public float timer;
	public bool timerPauzed = false;

	public SnapShotHandler FadeEffect;

	private IdentifyModel[] IdentifyModels;

	// Use this foritialization
	void Start () {
		if (isMainMenu) {
			PlayerPrefs.SetInt ("isVR", isVR ? 1 : 0);
		} else {
			int isVRPlayer = PlayerPrefs.GetInt ("isVR");
			Debug.Log (isVRPlayer);
			if (isVRPlayer == 1) {
				isVR = true;
			} else if (isVRPlayer == 0) {
				isVR = false;
			}
		}
		Cursor.lockState = CursorLockMode.Confined;
		QualitySettings.antiAliasing = 2;
		ActivePlayerObject = isVR ? VRPlayerObject : PCPlayerObject;
		ActiveObjectiveUI = isVR ? VRObjectiveUI : PCObjectiveUI;
		if (isVR)
			VRController.SetActive (true);
		else
			PCController.SetActive (true);
		IdentifyModels = FindObjectsOfType<IdentifyModel> ();
		foreach(IdentifyModel IdentifyModel in IdentifyModels){
			Canvas[] Canvases = IdentifyModel.GetComponentsInChildren<Canvas> ();
			foreach (Canvas Canvas in Canvases) {
				if (isVR)
					Canvas.GetComponent<LookAt> ().target = ActivePlayerObject.GetComponent<Transform>();
				Canvas.gameObject.SetActive (false);
			}
		}
	}

	// Update is called once per frame
	void Update () {
		if(!timerPauzed)
			timer += Time.deltaTime;
		if (Input.GetKeyDown("space") || Input.GetKeyDown ("joystick button 5")) {
				// PlayerPrefs.SetString ("LevelName","_Scenes/Demo_Mike");
      			// SceneManager.LoadSceneAsync("_Scenes/LoadingScene");
      			//SceneManager.LoadScene (SceneManager.GetActiveScene ().buildIndex);
		}
		if (Input.GetKeyDown("tab") || OVRInput.GetDown(OVRInput.RawButton.LShoulder)) {
			setObjectiveUIStatus ();
		}
	}

	void setObjectiveUIStatus(){
		if (ActiveObjectiveUI) {
			if (ObjectiveUIStatus) {
				ActiveObjectiveUI.SetActive (false);
				ObjectiveUIStatus = false;
			} else {
				ActiveObjectiveUI.SetActive (true);
				ObjectiveUIStatus = true;
			}
		}
	}

	public void endGame(){
		timerPauzed = true;
		PlayerPrefs.SetString("score", string.Format ("{0}:{1:00}", (int)timer / 60, (int)timer % 60));

		StartCoroutine (endGameRoutine ());
	}

	IEnumerator endGameRoutine() {
		yield return new WaitForSeconds(1);
		SceneManager.LoadSceneAsync(EndScene);
	}


	public GameObject getActivePlayerObject(){
		return ActivePlayerObject;
	}
}
