﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EndSceneController : MonoBehaviour {

	public string startScene;
	// Update is called once per frame
	void Update () {
		if (Input.GetMouseButtonDown (0) || Input.GetKeyDown ("joystick button 0")) {
			SceneManager.LoadSceneAsync("_Scenes/" + startScene);
		}
	}
}
