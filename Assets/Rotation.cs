﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotation : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update() 
	{
		// Rotate the object around its local X axis at 10 degree per second
		transform.Rotate(Vector3.back * 10 * Time.deltaTime);
	}
}